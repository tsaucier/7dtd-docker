# Ansible Role: 7 Days to Die

## Overview
This Ansible role sets up and manages a Docker container running the 7 Days to Die server using the `vinanrra/7dtd-server` Docker image.
The role performs the following tasks:
* Uses an externally managed Docker image (`vinanrra/7dtd-server`) with the recommended configuration.
* Prepares the host server by creating the necessary directories and setting the correct permissions.
* Configures and adds the Docker network required for the container.

## Requirements
* Ansible 2.9 or higher
* Docker community install via Ansible Galaxy: `ansible-galaxy collection install community.docker`
* Docker installed on the host machine

## Role Variables
You can customize the role using the following variables:

| Variable                  | Default Value                                | Description                       |
|:--------------------------|:---------------------------------------------|:----------------------------------|
| sdtd_server_name	         | 7dtd-server                                  | The name of the Docker container  |
| sdtd_server_image         | `vinanrra/7dtd-server`                       | The Docker image to use           |
| sdtd_server_image_version | `latest`                                     | The Docker image to use           |
| sdtd_server_ports         | See [defaults/main.yml](./defaults/main.yml) | Ports to expose                   |
| sdtd_server_volume_mounts | See [defaults/main.yml](./defaults/main.yml) | Volumes to mount in the container |
| sdtd_docker_network       | 7dtd                                         | Docker network for the container  |

## Usage
Referencing this Ansible role can be done in several ways, depending on your setup and requirements. Here's how you can do it:

### 1. Using Ansible Galaxy:
To install a role from a git repo, you can leverage the `requirements.yml` for Ansible Galaxy:
```yaml
# requirements.yml
---
roles:
  - name: 7daystodie
    src: git+https://gitlab.com/tsaucier/7dtd-ansible-role.git
    version: master # branch or tag: it is recommended to use a tagged version
```

Then running `ansible-galaxy install -r requirements.yml` to install the role.

### 2. Using `git submodule`:
If your Ansible playbook repository is also a Git repository, you can add the external role as a submodule:
```sh
git submodule add https://gitlab.com/tsaucier/7dtd-ansible-role.git roles/7daystodie
```

Then, you can reference the role in your playbook as if it were a local role:
```yaml
# playbook.yml
- hosts: localhost
  roles:
    - 7daystodie
```

### 3. Cloning the role directly into the roles directory:
You can clone the role repository directly into the `roles` directory of your Ansible playbook repository:
```sh
git clone https://gitlab.com/tsaucier/7dtd-ansible-role.git roles/7daystodie
```

Then, reference the role in your playbook:
```yaml
# playbook.yml
- hosts: localhost
  roles:
    - 7daystodie
```

## Pre-commit
To maintain code quality, we use pre-commit hooks. Before committing changes, ensure you have the following dependencies installed:
* Install [pre-commit](https://pre-commit.com/)
* Install [ansible-lint](https://ansible.readthedocs.io/projects/lint/installing/)

Once installed, you can run the pre-commit checks with:
```shell
pre-commit run -a
```

## License
This role is licensed under the Apache 2.0 License. See the [LICENSE](LICENSE) file for details.

## Author Information
This role was created by Travis Saucier.
